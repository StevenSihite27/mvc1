<form action="<?= BASE_URL."index.php?r=home/insertbarang" ?>" method="POST">
  <div class="mb-3">

    <label for="inputNama" class="form-label">Nama Barang</label>
    <input name= "nama" type="text" class="form-control" id="inputNama" aria-describedby="namaHelp">
    <small id="namaHelp" class="form-text">Isikan nama barang</div>

    <label for="inputJumlah" class="form-label">Jumlah</label>
    <input name= "qty" type="text" class="form-control" id="inputJumlah" aria-describedby="jumlahHelp">
    <small id="jumlahHelp" class="form-text">Isikan jumlah barang</div>

  </div>
  <button type="submit" class="btn btn-primary">Simpan</button>
</form>